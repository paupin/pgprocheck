package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	"crypto/md5"
	"database/sql"

	"github.com/lib/pq"
	"gopkg.in/yaml.v2"
)

// Function represents a function
type Function struct {
	Schema, Name  string
	Input, Output string
	Hash          string
}

// Compare two functions and return -1 (if `f` comes before), 1 (if it comes
// after) or 0 (if both are equal).
func (f Function) Compare(other Function) int {
	for _, pair := range [][2]string{
		{f.Schema, other.Schema},
		{f.Name, other.Name},
		{f.Input, other.Input},
		{f.Output, other.Output},
		{f.Hash, other.Hash},
	} {
		if pair[0] < pair[1] {
			return -1
		} else if pair[0] < pair[1] {
			return 1
		}
	}
	return 0
}

// FunctionList is a list of functions
type FunctionList []Function

func (fl FunctionList) Len() int { return len(fl) }
func (fl FunctionList) Less(i, j int) bool {
	return fl[i].Compare(fl[j]) == -1
}
func (fl FunctionList) Swap(i, j int) {
	fl[i], fl[j] = fl[j], fl[i]
}

// Named returns all functions with that name, from the list
func (fl FunctionList) Named(name string) FunctionList {
	res := FunctionList{}
	for _, f := range fl {
		if f.Name == name {
			res = append(res, f)
		}
	}
	return res
}

type connectionParams struct {
	User    string
	Host    string
	DBName  string
	SSLMode string
	Port    int
}

func (cs connectionParams) valid() bool {
	return cs.Host != ""
}

func (cs connectionParams) connectionString() string {
	sslmode := cs.SSLMode
	if sslmode == "" {
		sslmode = "disable"
	}
	s := fmt.Sprintf("user=%s dbname=%s host=%s sslmode=%s",
		cs.User,
		cs.DBName,
		cs.Host,
		sslmode,
	)
	if cs.Port != 0 {
		s += fmt.Sprintf(" port=%d", cs.Port)
	}
	return s
}

func loadFunctions(connectionString string) (FunctionList, error) {
	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		return nil, fmt.Errorf("connecting: %v", err)
	}
	defer db.Close()
	fmt.Printf("connected to %s\n", connectionString)

	rows, err := db.Query(`
		select N.nspname,
			P.proname,
			pg_catalog.pg_get_function_arguments(P.oid),
			pg_catalog.pg_get_function_result(P.oid),
			P.prosrc
		from
			pg_catalog.pg_proc P
			left join pg_catalog.pg_namespace N on (N.oid = P.pronamespace)
		where N.nspname = any($1)
		order by 1, 2, 3, 4;
	`, pq.Array(cfg.Schemas))
	if err != nil {
		return nil, fmt.Errorf("querying: %v", err)
	}

	functions := FunctionList{}
	for rows.Next() {
		var f Function
		var source string
		if err := rows.Scan(&f.Schema, &f.Name, &f.Input, &f.Output, &source); err != nil {
			return nil, fmt.Errorf("scanning: %v", err)
		}
		h := md5.New()
		io.WriteString(h, source)
		f.Hash = fmt.Sprintf("%x", h.Sum(nil))
		functions = append(functions, f)
	}
	return functions, nil
}

type environmentList map[string]connectionParams

type configuration struct {
	Ignore       []string
	Schemas      []string
	Environments environmentList
}

func (cfg *configuration) load(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("opening config: %v", err)
	}
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return fmt.Errorf("reading config: %v", err)
	}
	str := os.ExpandEnv(string(data))
	d := yaml.NewDecoder(strings.NewReader(str))
	d.SetStrict(true)
	if err := d.Decode(cfg); err != nil {
		return fmt.Errorf("decoding config: %v", err)
	}

	for key, c := range cfg.Environments {
		if c.Host == "" {
			return fmt.Errorf("%s: empty host", key)
		}
	}

	return nil
}

func abort(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
	fmt.Fprint(os.Stderr, "\n")
	os.Exit(1)
}

var cfg configuration

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("%s <config-file>\n", os.Args[0])
		os.Exit(1)
	}

	configFilename := os.Args[1]
	if err := cfg.load(configFilename); err != nil {
		abort("loading %s: %v", configFilename, err)
	}

	functionsByEnv := map[string]FunctionList{}
	f, err := os.Open("functions.yaml")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	if err := yaml.NewDecoder(f).Decode(functionsByEnv); err != nil {
		panic(err)
	}
	for key, params := range cfg.Environments {
		fx, err := loadFunctions(params.connectionString())
		if err != nil {
			abort("loading %s functionsByEnv: %v", key, err)
		}
		sort.Sort(fx)
		functionsByEnv[key] = fx
	}

	// sort environment list, to get a consistent output
	allEnvNames := []string{}
	for env := range cfg.Environments {
		allEnvNames = append(allEnvNames, env)
	}
	sort.Strings(allEnvNames)

	// get all known function names
	allFunctionNames := []string{}
	seen := map[string]bool{}
	for _, env := range allEnvNames {
		for _, f := range functionsByEnv[env] {
			if !seen[f.Name] {
				seen[f.Name] = true
				allFunctionNames = append(allFunctionNames, f.Name)
			}
		}
	}
	sort.Strings(allFunctionNames)

	ignore := func(name string) bool {
		for _, n := range cfg.Ignore {
			if n == name {
				return true
			}
		}
		return false
	}

	// diff everything; for every function, see in which environments it's present
	for _, name := range allFunctionNames {
		if ignore(name) {
			continue
		}

		different := false
		matches := map[string]FunctionList{}
		for _, env := range allEnvNames {
			matches[env] = functionsByEnv[env].Named(name)
			if len(matches[env]) != 1 {
				different = true
			}
		}
		if !different {
			firstEnv, otherEnvs := allEnvNames[0], allEnvNames[1:]
			for _, otherKey := range otherEnvs {
				first, other := matches[firstEnv][0], matches[otherKey][0]
				if first.Compare(other) != 0 {
					different = true
					break
				}
			}
		}

		if !different {
			continue
		}
		fmt.Printf("\nfound differences on %s:\n", name)
		for _, env := range allEnvNames {
			for _, m := range matches[env] {
				fmt.Printf("- %-6s [%s] %6s.%s(%s) %s\n",
					env,
					m.Hash,
					m.Schema,
					m.Name,
					m.Input,
					m.Output,
				)
			}
		}
	}
}
