# pgprocheck

This will check the consistency between Postgres stored procedures in
different hosts. It's useful to compare changes between development,
production and QA environments, for instance.

To run it, create a configuration file, similar to this:

```yaml
schemas:
  - public
  - other

ignore:
  - function_name_1
  - function_name_2

environments:
  master:
    user: read-only-user
    dbname: master-dbname
    host: master-hostname
  slave:
    user: read-only-user
    dbname: slave-dbname
    host: slave-hostname
```

Then run `go run ./cmd/pgprocheck config.yaml`.
